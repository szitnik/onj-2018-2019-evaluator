from flask import Flask, url_for
from flask import json
from flask import request
from flask import Response

app = Flask(__name__)

@app.route('/predict', methods = ["POST"])
def api_articles():
    print("Request: " + json.dumps(request.json))

    data = {
        "score": 1,
        "probability": None
    }
    js = json.dumps(data)

    return Response(js, status=200, mimetype='application/json')


if __name__ == '__main__':
    app.run(port=8080)