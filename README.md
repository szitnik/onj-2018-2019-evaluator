# ONJ 2018/2019 ImapBook evaluator

This repository contains an official evaluator script for ImapBook models evaluation.

Script `onj-eval.py` requires at least one parameter (test file), which is a CSV file of IMapBook example questions and answers. The script calls an HTTP server for all the three model types and outputs F scores accordingly.

Script `onj-dummy-server.py` is just a simple echo server for development purposes only.